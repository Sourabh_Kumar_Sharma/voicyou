<html>
<head>
<style>
.a1 /*main comment box */
{
position:relative;
top:30px;
word-wrap: break-word;
display:block;
border-style:solid;
border-width:2px;
border-color:gray;
width:400px;
background-color:lightblue;
border-radius:5px;
}
.navi1
{
background-image:url("navigation-bar.png");

height:60px;
}
.navisub1 /* odd number is used for the heading purpose */
{
background-color:red;
background-size:20px 20px;
background-repeat:no-repeat;
position:absolute;
left:15px;
border-width:1px;
text-decoration:none;
border-style:solid;
border-color:yellow;
width:100px;
height:58px;
}
.navisub2 /* even number is used for the purpose of sun heading in the navigation bar */
{
display:none;
}

.navisub1:hover+.navisub2
{
display:block;
position:absolute;
top:80px;
left:15px;
}
.navisub2:hover /*should not vanish once the mouse moves out from main menu to the drop-down list */
{
display:block;
position:absolute;
top:80px;
left:15px;
}

.navisub3
{
text-decoration:none;
}
.navisub4
{
color:white;
}
.navisub5
{
font-size:22px;
position:absolute;
top:15px;
color:white;
}
.navisub6
{
background-color:blue;
background-size:20px 20px;
background-repeat:no-repeat;
position:absolute;
left:145px;
border-width:1px;
text-decoration:none;
border-style:solid;
border-color:yellow;
width:100px;
height:58px;
}
.navisub7
{
font-size:22px;
color:white;
position:absolute;
top:15px;
}
.navisub8
{
display:none;
}

.navisub6:hover+.navisub8
{
display:block;
position:absolute;
top:80px;
left:145px;
}
.navisub8:hover
{
display:block;
position:absolute;
top:80px;
left:145px;
}

.navisub9
{
background-color:green;
background-size:20px 20px;
background-repeat:no-repeat;
position:absolute;
left:275px;
border-width:1px;
text-decoration:none;
border-style:solid;
border-color:yellow;
width:100px;
height:58px;
}
.navisub10
{
font-size:22px;
position:absolute;
top:15px;
color:white;
}
.navisub11
{
display:none;
}
.navisub9:hover+.navisub11
{
display:block;
position:absolute;
left:275px;
top:80px;
}
.navisub11:hover
{
display:block;
position:absolute;
left:275px;
top:80px;
}

.navisub12
{
background-color:gray;
background-size:20px 20px;
background-repeat:no-repeat;
position:absolute;
left:405px;
border-width:1px;
text-decoration:none;
border-style:solid;
border-color:yellow;
width:100px;
height:58px;
}

.navisub13
{
font-size:22px;
position:absolute;
top:0px;
color:white;
}

.navisub14
{
display:none;
}

.navisub12:hover+.navisub14
{
display:block;
position:absolute;
left:405px;
top:85px;
}
.navisub14:hover
{
display:block;
position:absolute;
left:405px;
top:85px;
}

.navisub15
{
background-color: 	#C0C0C0;
background-size:20px 20px;
background-repeat:no-repeat;
position:absolute;
left:535px;
border-width:1px;
text-decoration:none;
border-style:solid;
border-color:yellow;
width:100px;
height:58px;
}
.navisub16
{
font-size:22px;
position:absolute;
top:0px;
color:white;
}
.navisub17
{
display:none;
}
.navisub15:hover+.navisub17
{
display:block;
position:absolute;
left:535px;
top:85px;
}
.navisub17:hover
{
display:block;
position:absolute;
left:535px;
top:85px;
}
.navisub18
{
background-color:orange;
background-size:20px 20px;
background-repeat:no-repeat;
position:absolute;
left:665px;
border-width:1px;
text-decoration:none;
border-style:solid;
border-color:yellow;
width:100px;
height:58px;
}
.navisub19
{
font-size:22px;
position:absolute;
top:0px;
color:white;
}
.navisub20
{
display:none;
}
.navisub18:hover+.navisub20
{
display:block;
position:absolute;
left:665px;
top:85px;
}
.navisub20:hover
{
display:block;
position:absolute;
left:665px;
top:85px;
}


.a1:after
{
display:block;
background-color:red;
height:20px;
}
.navili1  /* for the menu list's items border */
{
color:white;
border-style:solid;
border-color:yellow;
border-width:1px;
text-decoration:none;
width:100px;
background-color:red;

}

.navili1:hover  /* for the menu list's items border */
{
color:white;
border-style:solid;
border-color:yellow;
border-width:1px;
text-decoration:none;
width:100px;
background-color:indigo;

}

.a1:hover
{
display:block;
border-style:solid;
border-width:2px;
border-color:green;
background-color:lightgray;

}
.a2 /* for options button */
{
width:10px;
height:10px;
display:block;
position:absolute;
top:0px;
left:390px;
}
.a3
{
display:none;
position:absolute;
top:0px;
transition: left 2s;
-webkit-transition:left 2s; /* Safari */
}
.a2:hover+.a3
{
opacity:0.5;
display:block;
position:relative;
left:390px;
top:0px;
}
.a3:hover
{
opacity:0.5;
display:block;
position:relative;
left:400px;
top:40px;
width:100px;

}

.comlidiv /*for div of comment like dislike */
{
display:block;
position:absolute;
top:0px;
background-color:red;
}


.maintri1   /* all elements starting with tri for showing label with a triangle */
{
font-size:11px;




background-color:red;
text-decoration:none;

}
.maintri2
{
display:none;

}

.maintri1:hover+.maintri2  /*show label for like button hover, + indicates immediate child */
{
color:white;
display:block;
font-size:11px;
width:115px;
position:absolute;
top:800px;


background-color:indigo;
left:140px;
}
.maintri1:hover+.maintri2:after  /* to create triangle arrow */
{
content: "";
display:block;

width:0px;
height:0px;
border-color:solid lightgray;
border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	
	border-top: 5px solid indigo;
	
	position:absolute;
	
	top:13px;
	left:10px;
}

.maintri3   /* all elements starting with tri for showing label with a triangle */
{
font-size:11px;
background-color:red;
text-decoration:none;
}
.maintri4
{
display:none;
}

.maintri3:hover+.maintri4  /*show label for like button hover, + indicates immediate child */
{
color:white;
display:block;
font-size:11px;
width:117px;
position:absolute;
top:800px;
background-color:indigo;
left:60px;
}
.maintri3:hover+.maintri4:after  /* to create triangle arrow */
{
content: "";
display:block;

width:0px;
height:0px;
border-color:solid lightgray;
border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	
	border-top: 5px solid indigo;
	
	position:absolute;
	
	top:13px;
	left:10px;
}

.maintri5   /* all elements starting with tri for showing label with a triangle */
{
font-size:11px;
background-color:red;
text-decoration:none;
}
.maintri6
{
display:none;
}

.maintri5:hover+.maintri6  /*show label for like button hover, + indicates immediate child */
{
color:white;
display:block;
font-size:11px;
width:95px;
position:absolute;
top:800px;
background-color:indigo;
left:1px;
}
.maintri5:hover+.maintri6:after  /* to create triangle arrow */
{
content: "";
display:block;

width:0px;
height:0px;
border-color:solid lightgray;
border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	
	border-top: 5px solid indigo;
	
	position:absolute;
	
	top:13px;
	left:10px;
}



.maintri7   /* all elements starting with tri for showing label with a triangle */
{
font-size:11px;
background-color:red;
text-decoration:none;
}
.maintri8
{
display:none;
}

.maintri7:hover+.maintri8 /*show label for like button hover, + indicates immediate child */
{
color:white;
display:block;
font-size:11px;
width:105px;
position:absolute;
top:800px;
background-color:indigo;
left:220px;
}
.maintri7:hover+.maintri8:after  /* to create triangle arrow */
{
content: "";
display:block;

width:0px;
height:0px;
border-color:solid lightgray;
border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	
	border-top: 5px solid indigo;
	
	position:absolute;
	
	top:13px;
	left:10px;
}

.tri1   /* all elements starting with tri for showing label with a triangle */
{
font-size:11px;
width:50px;
display:block;
position:absolute;

left:160px;
text-decoration:none;

}
.tri2
{
display:none;

}

.tri1:hover+.tri2  /*show label for like button hover, + indicates immediate child */
{
color:white;
display:block;
font-size:11px;
width:100px;
position:absolute;
top:85px;


background-color:indigo;
left:140px;
}
.tri1:hover+.tri2:after  /* to create triangle arrow */
{
content: "";
display:block;

width:0px;
height:0px;
border-color:solid lightgray;
border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	
	border-top: 5px solid indigo;
	
	position:absolute;
	
	top:13px;
	left:40px;
}

.tri3
{
font-size:11px;
display:block;
position:absolute;
width:57px;

left:80px;
text-decoration:none;

}
.tri4
{
display:none
}
.tri3:hover+.tri4   	/*show label for dislike button hover */
{
color:white;
display:block;
font-size:11px;
width:115px;
position:absolute;
top:80px;

height:13px;
display:block;
background-color:indigo;
left:60px;
}
.tri3:hover+.tri4:after
{
content: "";
display:block;

width:0px;
height:0px;
border-color:solid lightgray;
border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	
	border-top: 5px solid indigo;
	
	position:absolute;
	
	top:-1px;
	left:50px;
}

.tri5
{
font-size:11px;
display:block;
position:absolute;
width:50px;

left:10px;
text-decoration: none;
}
.tri6
{
display:none
}
.tri5:hover+.tri6	/*show label for reply button hover */
{
color:white;
display:block;
font-size:11px;
width:95px;
height:13px;
position:absolute;
top:80px;


display:block;
background-color:indigo;
left:10px;
}
.tri5:hover+.tri6:after
{
content: "";
display:block;

width:0px;
height:0px;
border-color:solid lightgray;
border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	
	border-top: 5px solid indigo;
	
	position:absolute;
	
	top:12px;
	left:0px;
}


.img2
{
width:400px;
height:400px;
}
.div2
{
border-style:solid;border-width:1px;border-color:gray;width:400px;
}
.tt1
{
color:blue;
}
.in1
{
width:0px;
height:0px;
border-style:solid;
border-color:white;

}
.ftr
{
display:block;
}
.reptxt1
{
border-style:solid;
border-style:solid;
border-color:red;
position:absolute;
left:10px;
}
.txtarea1
{
border-style:solid;border-color:blue;
width:404px;
height:25px;
}
.maindiv1
{
position:relative;
left:300px;
border-style:solid;
border-width:1px;
border-color:gray;
width:403px
}
.mainsubdiv1
{
position:relative;
left:00px;
border-style:solid;
border-width:1px;
border-color:gray;
width:403px
}
.mainlike1	/*for the text 'like' for the pic */
{
font-size:11px;
text-decoration:none;
}
.maindislike1	/*for the text 'dislike' for the pic */
{
font-size:11px;
text-decoration:none;
}
.maincomment1
{
font-size:11px;
text-decoration:none;
}
.mainshare1
{
font-size:11px;
text-decoration:none;
}
.mainlike1no    /*for the no.of likes for the pic */
{
font-size:11px;
text-decoration:none;
position:absolute;
left:0px;
}
.maindislike1no 	/*for the no.of dislikes for the pic */
{
font-size:11px;
text-decoration:none;
position:absolute;
left:80px;
}

.maincomment1no /*for the no.of comment for the pic */
{
font-size:11px;
text-decoration:none;
}
</style>
<script>

var liked,disliked,commented,shared,ckey,comlikedv,comdislikedv;//comlikedv for the comment that is liked
function comreply(comreply)
{ 
var reptxtarea=document.createElement('textarea');
document.getElementById();
window.alert(comreply);
var comrepnoid=comreply+'seereno';                          //id for no.of replies
var comrepno=document.getElementById(comrepnoid).innerHTML; // no.of replies
window.alert(comrepno);
var replytxt1=document.createElement('input'); //input to carry no.of replies
replytxt1.type="text";
replytxt1.name="redisliketxtt1";
replytxt1.value=redislikeno; //no.of replies

}
function comdisliked(comdislikedv)
{
var redislikenoid=comdislikedv+"no";
var redislikeno=document.getElementById(redislikenoid).innerHTML; //no.of dislikes
window.alert(redislikeno);

var redislikeform1=document.createElement('form');
redislikeform1.method="post";
redislikeform1.action="http://192.168.150.2/mod.php";

var ussubid=document.getElementById('uid1').innerHTML;//carry userid
var usid=document.createElement('input');
usid.type="text";
usid.value=ussubid;
usid.name="userid";

var redisliketxt1=document.createElement('input'); //input to carry no.of dislikes
redisliketxt1.type="text";
redisliketxt1.name="redisliketxtt1";
redisliketxt1.value=redislikeno; //no.of dislikes

var redisliketxt2=document.createElement('input'); 
redisliketxt2.type="text";
redisliketxt2.name="redisliketxtt2";
redislikenoid='id='+'"'+redislikenoid+'"';
redisliketxt2.value=redislikenoid; //id of no.of dislikes

var redisliketxt3=document.createElement('input'); 
redisliketxt3.type="text";
redisliketxt3.name="redisliketxtt3";
comdislikedv2=comdislikedv+"e";
comdislikedv=comdislikedv+"e";
comdislikedv='id='+'"'+comdislikedv+'"';
redisliketxt3.value=comdislikedv; //id of the dislike button

var redisliketxt4=document.getElementById(comdislikedv2).innerHTML; //get the text 'dislike' or 'u dislike'
window.alert(redisliketxt4);
if(redisliketxt4=="dislike")
{
redisliketxt4="u dislike";
document.getElementById(comdislikedv2).innerHTML=redisliketxt4;
}
else
{
redisliketxt4="dislike";
document.getElementById(comdislikedv2).innerHTML=redisliketxt4;
}

var redisliketxt5=document.createElement('input'); //input to carry the converted text 'dislike' or 'u dislike'
redisliketxt5.type="text";
redisliketxt5.name="redislike";
redisliketxt5.value=redisliketxt4;

redislikeform1.appendChild(usid);
redislikeform1.appendChild(redisliketxt1);
redislikeform1.appendChild(redisliketxt2);
redislikeform1.appendChild(redisliketxt3);
redislikeform1.appendChild(redisliketxt5);
document.body.appendChild(redislikeform1);
redislikeform1.submit();
}
function comliked(comlikedv) //function for the comments that are liked
{
var relikenoid=comlikedv+"no";//id of the like of the replied comment
var relikeno=document.getElementById(relikenoid).innerHTML; //no.of likes
var relikeform=document.createElement('form');
relikeform.method="post";
window.alert(relikeno);
relikeform.action="http://192.168.150.2/mod.php";
var ussubid=document.getElementById('uid1').innerHTML;//carry userid
var usid=document.createElement('input');
usid.type="text";
usid.value=ussubid;
usid.name="userid";

var reliketxt1=document.createElement('input'); 
reliketxt1.type="text";
reliketxt1.name="reliketxtt1";
reliketxt1.value=relikeno; //no.of likes

var reliketxt2=document.createElement('input'); 
reliketxt2.type="text";
reliketxt2.name="reliketxtt2";
relikenoid='id='+'"'+relikenoid+'"';
reliketxt2.value=relikenoid; //id of no.of likes

var reliketxt3=document.createElement('input'); 
reliketxt3.type="text";
reliketxt3.name="reliketxtt3";
comlikedv2=comlikedv+"e";
comlikedv=comlikedv+"e";
comlikedv='id='+'"'+comlikedv+'"';
reliketxt3.value=comlikedv; //id of the like button

var reliketxt4=document.getElementById(comlikedv2).innerHTML; //get the text 'like' or 'u like'
window.alert(reliketxt4);
if(reliketxt4=="like")
{
reliketxt4="u like";
document.getElementById(comlikedv2).innerHTML=reliketxt4;
}
else
{
reliketxt4="like";
document.getElementById(comlikedv2).innerHTML=reliketxt4;
}
window.alert(reliketxt4);
var reliketxt5=document.createElement('input'); //input to carry the converted text 'like' or 'u like'
reliketxt5.type="text";
reliketxt5.name="relike";
reliketxt5.value=reliketxt4;

relikeform.appendChild(usid);
relikeform.appendChild(reliketxt1);
relikeform.appendChild(reliketxt2);
relikeform.appendChild(reliketxt3);
relikeform.appendChild(reliketxt5); //text 'like' or 'u dislike'
document.body.appendChild(relikeform);
relikeform.submit();
}

function like(liked)
{
var liketxt;
var chliket=liked+"t";
var chlike=document.getElementById(chliket).innerHTML;

if(chlike=="like")
{

liketxt="like";
document.getElementById(chliket).innerHTML=liketxt;
window.alert(liketxt);
}
/*else if(chlike=="u like")
{
liketxt="u like";
document.getElementById(chliket).innerHTML=liketxt;
window.alert(liketxt);
} */ //this is not needed here because we are not changing the like text here
var liketxts=document.createElement('input');
liketxts.type="text";
liketxts.name="liketxtsub";
liketxts.value=liketxt;

var ussubid=document.getElementById('uid1').innerHTML;


var likeid=document.getElementById(liked).id;
var likesub=document.createElement('form');
likesub.method="post";
likesub.action="http://192.168.150.2/mod.php";

var usid=document.createElement('input');
usid.type="text";
usid.value='<?php echo $a ?>';
usid.name="userid";

var likdocpath=document.createElement('input');
likdocpath.type='text';
likdocpath.name='likpath';
likdocpath.value='<?php echo __FILE__ ?>';
var uslike=document.createElement('input');
uslike.type="text";
uslike.value=likeid;
uslike.name="liked";

var likeid2=liked+"e";
var likeva=document.getElementById(likeid2).innerHTML;
var uslikeva=document.createElement('input');
uslikeva.type="text";
uslikeva.value=likeva;
uslikeva.name="likeval";


likesub.appendChild(likdocpath);
likesub.appendChild(usid);
likesub.appendChild(uslike);
likesub.appendChild(uslikeva);
likesub.appendChild(liketxts);
document.body.appendChild(likesub);
likesub.submit();
}



function dislike(disliked)
{
var disliketxt;
var dischliket=disliked+"t";
var dischlike=document.getElementById(dischliket).innerHTML;

if(dischlike=="dislike")
{
disliketxt="dislike";
document.getElementById(dischliket).innerHTML=disliketxt;

}
else if(dischlike=="u dislike")
{
disliketxt="u dislike";
document.getElementById(dischliket).innerHTML=disliketxt;

}
var disliketxts=document.createElement('input');
disliketxts.type="text";
disliketxts.name="disliketxtsub";
disliketxts.value=disliketxt;

var ussubid=document.getElementById('uid1').innerHTML;


var dislikeid=document.getElementById(disliked).id;
var dislikesub=document.createElement('form');
dislikesub.method="post";
dislikesub.action="http://192.168.150.2/mod.php";

var usid=document.createElement('input');
usid.type="text";
usid.value=ussubid;
usid.name="userid";

var disuslike=document.createElement('input');
disuslike.type="text";
disuslike.value=dislikeid;
disuslike.name="disliked";

var dislikeid2=disliked+"e";
var dislikeva=document.getElementById(dislikeid2).innerHTML;
var disuslikeva=document.createElement('input');
disuslikeva.type="text";
disuslikeva.value=dislikeva;
disuslikeva.name="dislikeval";
window.alert(dislikeva);


dislikesub.appendChild(usid);
dislikesub.appendChild(disuslike);
dislikesub.appendChild(disuslikeva);
dislikesub.appendChild(disliketxts);
document.body.appendChild(dislikesub);
dislikesub.submit();
}
function comment(comment)
{
var maincomdiv=document.createElement('div');//main div container
var comdiv=document.createElement('div');//comment div container
comdiv.setAttribute("style","word-wrap:break-word;border-style:solid;border-color:gray;width:410px;height:80px;border-width:1px;background-color:gray;");


var compropic=document.createElement('img'); //profile pic of commenter
compropic.src="http://192.168.150.2/1/profile.png";//here 1 should be replaced with userid folder that stores profile pic
compropic.width="40";
compropic.height="40";

var comdel=document.createElement('input');
comdel.type="image";
 
comdel.src="http://192.168.150.2/delete2.png";
comdel.width="20";
comdel.height="20";
comdel.setAttribute("style","position:relative;top:0px;left:370px;");

var comtextarea=document.createElement('textarea');
comtextarea.setAttribute("cols","40");
comtextarea.setAttribute("rows","3");
comtextarea.setAttribute("style","position:relative;top:0px;left:-20px;border-style:solid;border-color:white;");
var comliketxt=document.createTextNode("like");//text for comment
var comlikett=document.createElement('tt');
comlikett.appendChild(comliketxt);


var comlike=document.createElement('a'); //like for comment
comlike.appendChild(comlikett);
comlike.href="javascript:void(0)";
comlike.setAttribute("style","position:relative;top:0px;left:50px;");

var discomliketxt=document.createTextNode("dislike");//text for comment
var discomlikett=document.createElement('tt');
discomlikett.appendChild(discomliketxt);
var discomlike=document.createElement('a'); //like for comment
discomlike.appendChild(discomlikett);
discomlike.href="javascript:void(0)";
discomlike.setAttribute("style","position:relative;top:0px;left:60px;");

var comreplytxt=document.createTextNode("reply");//text for comment
var comreplytt=document.createElement('tt');
comreplytt.appendChild(comreplytxt);


var comreply=document.createElement('a'); //like for comment
comreply.onclick=function(){comreply.appendChild(comlikett)};
comreply.appendChild(comreplytt);
comreply.href="javascript:void(0)";
comreply.setAttribute("style","position:relative;top:0px;left:70px;");


comdiv.appendChild(compropic);
comdiv.appendChild(comdel);
comdiv.appendChild(comtextarea);
comdiv.appendChild(comlike);
comdiv.appendChild(discomlike);
comdiv.appendChild(comreply);
maincomdiv.appendChild(comdiv);
document.getElementById('fdiv1').appendChild(maincomdiv);
}

function comtxtsub(commented,ckey)
{
if(ckey.keyCode==13)
{

var ussubid=document.getElementById('uid1').innerHTML;

var cmtne=commented+"e"; //no.of comments id
var cmtnos= document.getElementById(cmtne).innerHTML; //total no.of comments

var cmttxt=document.getElementById(commented).value;

var cmtid=document.createElement('input');//comment id


cmtid.type="text";
cmtid.name="cmtidd";
cmtid.value=commented;

var usid=document.createElement('input');//user id input
usid.type="text";
usid.value=ussubid;
usid.name="userid";

var cmtnoss=document.createElement('input');//no.of comments
cmtnoss.type="text";
cmtnoss.name="cmtno";
cmtnoss.value=cmtnos;

var cmttxtt=document.createElement('input'); //comment text
cmttxtt.type="text";
cmttxtt.name="cmtxttt";
cmttxtt.value=cmttxt;

var form=document.createElement('form');
form.method="post";
form.action="http://192.168.150.2/mod.php";
form.appendChild(usid);
form.appendChild(cmtid);
form.appendChild(cmtnoss);
form.appendChild(cmttxtt);
document.body.appendChild(form);
form.submit();

}
}
function commentopt()
{
var divopt=document.createElement('div');
}

function loaddef()
{
 alert('This website is in beta state click on India option in Asia to see how this website works  in mozilla firefox or opera browser:)');

}
function getCountry(a)
{
alert(a);
document.location.href="http://www.voicyou.com/news/asia/"+a+"/"+a+".php";
alert('href');

}
</script>
</head>
<body onload='loaddef()'>
<img src="wel3.png" />
<div class="navi1" id="navi1"> <div class="navisub1"><a href="javascript:void(0);"><tt class="navisub5">Africa</tt></a>
				</div>
	<div class="navisub2">
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Algeria</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Angola</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)"> Benin</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Botswana</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Burkina</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Burundi</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Cameroon</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Cape Verde</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Central African</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Republic</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Chad</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Comoros</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Congo</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Congo</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">(Dem. Rep.)</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Djibouti</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Egypt</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Equatorial Guinea</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Eritrea</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Ethiopia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Gabon</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Gambia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Ghana</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Guinea</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Guinea-Bissau</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Ivory Coast</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Kenya</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Lesotho</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Liberia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Libya</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Madagascar</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Malawi</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Mali</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Mauritania</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Mauritius</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Morocco</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Mozambique</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Namibia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Niger</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Nigeria</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Rwanda</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Sao Tome</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">and Principe</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Senegal</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Seychelles</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Sierra Leone</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Somalia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">South Africa</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Sudan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Swaziland</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Tanzania</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Togo</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Tunisia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Uganda</code></a></div>
<div class="navili1"><a><a href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Zambia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Zimbabwe</code></a></div> </tt> </div> 

 <div class="navisub6"><a href="javascript:void(0);"><tt class="navisub7">Asia</tt></a>
				</div>
	<div class="navisub8">
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)" >Afghanistan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Bahrain</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)"> Bangladesh</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Bhutan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Brunei</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Burma (Myanmar)</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Cambodia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">China</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">East Timor</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0)"><code class="navisub4" onclick="getCountry(this.innerHTML)">India</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Indonesia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Iran</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Iraq</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Israel</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Japan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Jordan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Kazakhstan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Korea (north)</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4"  onclick="getCountry(this.innerHTML)">Korea (south)</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Kuwait</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Kyrgyzstan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Laos</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Lebanon</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Malaysia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Maldives</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Mongolia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Nepal</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Oman</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Pakistan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Philippines</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Qatar</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Russian
Federation</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Saudi Arabia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Singapore</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Sri Lanka</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Syria</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Tajikistan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Thailand</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Turkey</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4">Turkmenistan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">United Arab
Emirates</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Uzbekistan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Vietnam</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Yemen</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Seychelles</code></a></div>
 </tt> </div>

<div class="navisub9"><a href="javascript:void(0);"><tt class="navisub10">Europe</tt></a>
				</div>
	<div class="navisub11">
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Albania</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Andorra</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Armenia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Austria</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Azerbaijan</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Belarus</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Belgium</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Bosnia and Herzegovina</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Bulgaria</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Croatia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Cyprus</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Czech Republic</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Denmark</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Estonia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Finland</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">France</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Georgia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Germany</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Greece</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Hungary</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Iceland</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Ireland</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Italy</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Latvia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Liechtenstein</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Lithuania</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Luxembourg</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Macedonia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Malta</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Moldova</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Monaco</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Montenegro</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Netherlands</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Norway</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Poland</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Portugal</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Romania</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">San Marino</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Serbia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Slovakia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Slovenia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Spain</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Sweden</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Switzerland</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Ukraine</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">United Kingdom</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Vatican City</code></a></div>
</div> 

<div class="navisub12"><a href="javascript:void(0);"><tt class="navisub13">North America</tt></a>
				</div>
	<div class="navisub14">
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Antigua and Barbuda</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Bahamas</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Barbados</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Belize</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Canada</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Costa Rica</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Cuba</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Dominica</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Dominican Rep.</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">El Salvador</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Grenada</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Guatemala</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Haiti</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Honduras</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Jamaica</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Mexico</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Nicaragua</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Panama</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">St. Kitts & Nevis</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">St. Lucia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">St. Vincent &
the Grenadines</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Trinidad & Tobago</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">United States</code></a></div>
</div> 

<div class="navisub15"><a href="javascript:void(0);"><tt class="navisub16">South America</tt></a>
				</div>
	<div class="navisub17">
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)" >Argentina</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Bolivia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Brazil</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Chile</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Colombia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Ecuador</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Guyana</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Paraguay</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Peru</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Suriname</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Uruguay</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Venezuela</code></a></div>
</div>

<div class="navisub18"><a href="javascript:void(0);"><tt class="navisub19">Oceania</tt></a>
				</div>
	<div class="navisub20">
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)" >Australia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Fiji</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Kiribati</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Marshall Islands</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Micronesia</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Nauru</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">New Zealand</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Palau</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Papua New Guinea</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Samoa</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Solomon Islands</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Tonga</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Tuvalu</code></a></div>
<div class="navili1"><a class="navisub3" href="javascript:void(0);"><code class="navisub4" onclick="getCountry(this.innerHTML)">Vanuatu</code></a></div>
</div> 
</div>
<img src="globe.png" />

</body>
</html>
